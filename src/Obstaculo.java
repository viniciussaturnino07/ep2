import java.awt.Color;
import java.awt.Graphics;

public class Obstaculo {
	
	private int cordX;
	private int cordY;
	private int width;
	private int height;
	private String tipo;
	private int score;
	
	public Obstaculo(int cordX, int cordY, int tileSize, String tipo) {
		
		this.cordX = cordX;
		this.cordY = cordY;
		this.height = tileSize;
		this.width = tileSize;
		this.tipo = tipo;	
		
		if(tipo == "Obstaculo") {
			this.score = 0;
		}
		
	}

	public String getTipo() {
		return tipo;
	}



	public void setTipo(String tipo) {
		this.tipo = tipo;
	}



	public int getScore() {
		return score;
	}



	public void setScore(int score) {
		this.score = score;
	}

	public void tick() {
		
	}
	
	public void draw(Graphics g) {
		//Obstaculo
		if(tipo == "Obstaculo") {
			g.setColor(new Color(244, 76 ,0));
			g.fillRect(cordX*width, cordY*height, width, height);
		}
	}

	public int getCordX() {
		return cordX;
	}

	public void setCordX(int cordX) {
		this.cordX = cordX;
	}

	public int getCordY() {
		return cordY;
	}

	public void setCordY(int cordY) {
		this.cordY = cordY;
	}

}
