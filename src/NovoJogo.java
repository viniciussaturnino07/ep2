import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class NovoJogo {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NovoJogo window = new NovoJogo();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public NovoJogo() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 600, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblEscolhaOTipo = new JLabel("Escolha o tipo de cobra para o jogo");
		lblEscolhaOTipo.setForeground(Color.WHITE);
		lblEscolhaOTipo.setFont(new Font("Dialog", Font.BOLD, 20));
		lblEscolhaOTipo.setBounds(81, 12, 413, 49);
		frame.getContentPane().add(lblEscolhaOTipo);
		
		JButton btnComum = new JButton("Comum");
		btnComum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Main("Comum");
				frame.setVisible(false);
			}
		});
		btnComum.setBounds(65, 73, 117, 25);
		frame.getContentPane().add(btnComum);
		
		JButton btnKitty = new JButton("Kitty");
		btnKitty.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Main("Kitty");
				frame.setVisible(false);
			}
		});
		btnKitty.setBounds(220, 73, 117, 25);
		frame.getContentPane().add(btnKitty);
		
		JButton btnStar = new JButton("Star");
		btnStar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Main("Star");
				frame.setVisible(false);
			}
		});
		btnStar.setBounds(377, 73, 117, 25);
		frame.getContentPane().add(btnStar);
		
		JLabel lblNewLabel = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("menusecundario.png")).getImage();
		lblNewLabel.setIcon(new ImageIcon(img));
		lblNewLabel.setBounds(0, 0, 600, 300);
		frame.getContentPane().add(lblNewLabel);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
	}
}
