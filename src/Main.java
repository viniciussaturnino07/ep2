import javax.swing.JFrame;

public class Main {
	
	private static String tipo;
	
	public Main(String tipo) {
		
		JFrame frame = new JFrame();
		Game game = new Game(tipo);
		
		frame.add(game);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Snake Game");
		frame.setResizable(false);
		
		frame.pack();
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		
	}
	
	public static void main(String[] args) {
		
		new Main(tipo);
		
	}

}
