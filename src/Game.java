import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JPanel;

public class Game extends JPanel implements Runnable, KeyListener{
	
	private static final long serialVersionUID = 1L;
	
	private static final int WIDTH = 600;
	private static final int HEIGHT = 600;
	
	private Thread thread;
	
	private boolean run;
	
	private boolean right = true;
	private boolean left = false;
	private boolean up = false;
	private boolean down = false;
	
	private Snake s;
	private ArrayList<Snake> snake;
	private String tipo = "Comum";
	private int size = 5;
		
	private Fruit fruit;
	private long time;
	private ArrayList<Fruit> fruits;
	private Random r;

	
	private Obstaculo obstaculo;
	private ArrayList<Obstaculo> obstaculos;
	
	
	private int cordX = 10, cordY = 10;
	private int ticks = 0;
	private int score = 0;
	public Game(String tipo) {
		setFocusable(true);
		
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		addKeyListener(this);
		
		this.snake = new ArrayList<Snake>();
		this.fruits = new ArrayList<Fruit>();
		this.obstaculos = new ArrayList<Obstaculo>();
		
		this.r = new Random();
		this.tipo = tipo;
		
		start();
	}
	
	//Funcao responsavel pela inicializacao do jogo
	public void start() {
		run = true;
		thread = new Thread(this);
		thread.start();
	}
	
	//Funcao responsavel pela parada do jogo
	public void stop() {
		run = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		repaint();
	}
	
	
	//Funcao de movimentos do jogo
	public void tick() {
		if(snake.size() == 0) {
			s = new Snake(cordX, cordY, 10, tipo);
			snake.add(s);
		}
		ticks++;
		if(ticks > 500000) {
			if(right) {
				cordX++;
			}
			if(left) {
				cordX--;
			}
			if(up) {
				cordY--;
			}
			if(down) {
				cordY++;
			}
			ticks = 0;
			
			s = new Snake(cordX, cordY, 10, tipo);
			snake.add(s);
			
			if(snake.size() > this.size) {
				snake.remove(0);
			}
		}
		
		//Aleatoriedade das frutas
		if(fruits.size() == 0) {
			
			int aux = 0;
			
			int cordX1 = r.nextInt(59);
			int cordY1 = r.nextInt(59);
			
			int cordX2 = r.nextInt(59);
			int cordY2 = r.nextInt(59);
			
			for(int i=0; i<obstaculos.size(); i++) {
				if(cordX1 == obstaculos.get(i).getCordX() && cordY1 == obstaculos.get(i).getCordY()) {
					aux++;
				}
				if(cordX2 == obstaculos.get(i).getCordX() && cordY2 == obstaculos.get(i).getCordY()) {
					aux++;
				}
			}
			
			for(int i=0;i<snake.size();i++) {
				if(cordX2 == snake.get(i).getCordX() && cordY2 == snake.get(i).getCordY()) {
					aux++;
				}
				if(cordX2 == snake.get(i).getCordX() && cordY2 == snake.get(i).getCordY()) {
					aux++;
				}
				
			}

			
			if(aux == 0) {
				
				fruit = new Fruit(cordX1, cordY1, 10, "Simple");
				fruits.add(fruit);
				
				fruit = new Fruit(cordX2, cordY2, 10);
				fruits.add(fruit);
				fruits.add(fruit);
				
				time = System.currentTimeMillis();
				
				aux = 0;
			}
			
		}
		
		if(time+10000 == System.currentTimeMillis()) {
			fruits.removeAll(fruits);
		}
		
		
		//Colisao da cabeca da cobra com os 4 tipos de fruta
		for(int i=0; i<fruits.size(); i++) {
			if(cordX == fruits.get(i).getCordX() && cordY == fruits.get(i).getCordY()) {
				//Condicao para GAME OVER caso a cobra colida com o tipo Bomb
				if(fruits.get(i).getTipo() == "Bomb") {
					stop();
				}
				//Condicao para restaurar tamanho inicial da cobra caso colida com o tipo Decrease
				else if(fruits.get(i).getTipo() == "Decrease") {
					size = 5;
					while(snake.size()!=5) {
						snake.remove(0);
					}
				}
				//Condicao para ganhar o dobro de score quando colidir com o tipo Big
				else if(fruits.get(i).getTipo() == "Big") {
					size+=2;
				}
				//Condicao de crescimento para colisao com o tipo Simple
				else {
					size++;
				}
				
				if(s.getTipo()=="Star") {
					this.score += (fruits.get(i).getScore()*2);
				}
				else if(s.getTipo()=="Comum" || s.getTipo()=="Kitty") {
					this.score += fruits.get(i).getScore();
				}
				
				fruits.removeAll(fruits);
				i++;
			}
		}
		
		
		//Condicao de GAME OVER para colisao com o proprio corpo
		for(int i=0; i<snake.size(); i++) {
			if(cordX == snake.get(i).getCordX() && cordY == snake.get(i).getCordY()) {
				if(i!=snake.size()-1) {
					stop();
				}
			}
		}
		
		
		//Condicao de GAME OVER para colisao com a borda
		if(cordX < 0 || cordX > 59 || cordY < 0 || cordY > 59) {
			stop();
		}
	}
	
	public void paint(Graphics g) {
		g.clearRect(0, 0, WIDTH, HEIGHT);
		
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, WIDTH, HEIGHT);
				
		for(int i=0; i<WIDTH/10; i++) {
			g.drawLine(i*10, 0, i*10, HEIGHT);
		}
		for(int i=0; i<HEIGHT/10; i++) {
			g.drawLine(0, i*10, HEIGHT,i*10);
		}
		
		
		//Criacao dos obstaculos pelo mapa
				for(int i=10; i<30; i++) {
					obstaculo = new Obstaculo(40,i,10,"Obstaculo");
					obstaculos.add(obstaculo);
					obstaculo = new Obstaculo(i,30,10,"Obstaculo");
					obstaculos.add(obstaculo);
				}
				for(int i=10; i<40; i++) {
					obstaculo = new Obstaculo(40,i,10,"Obstaculo");
					obstaculos.add(obstaculo);
					obstaculo = new Obstaculo(i,15,10,"Obstaculo");
					obstaculos.add(obstaculo);
					obstaculo = new Obstaculo(i,20,10,"Obstaculo");
					obstaculos.add(obstaculo);
				}
				for(int i=0; i<10; i++) {
					obstaculo = new Obstaculo(10,i,10,"Obstaculo");
					obstaculos.add(obstaculo);
					obstaculo = new Obstaculo(i,95,70,"Obstaculo");
					obstaculos.add(obstaculo);
				}
				
				//Desenha os obstaculos no mapa
				for(int i=0; i<obstaculos.size();i++){
					obstaculos.get(i).draw(g);
				}
				//Casos de colisao com os obstaculos
				for(int i=0; i<obstaculos.size(); i++) {
					if(s.getTipo() != "Kitty") {
						if(cordX == obstaculos.get(i).getCordX() && cordY == obstaculos.get(i).getCordY()) {
							stop();
						}
					}
				}
		
		//Desenha a cobra no mapa
		for(int i=0; i<snake.size(); i++) {
			snake.get(i).draw(g);
		}
		//Desenha as frutas no mapa
		for(int i=0; i<fruits.size(); i++) {
			fruits.get(i).draw(g);
		}
		
		//Desenhando score na tela
		g.setColor(Color.WHITE);
		g.setFont(new Font("arial", Font.BOLD, 14));
		g.drawString("Score: "+ score, 10, 15);
		
		if(run == false) {
			g.setColor(Color.RED);
			g.setFont(new Font("arial", Font.BOLD, 50));
			g.drawString("GAME OVER!", 100, 500);
			
			g.setColor(Color.RED);
			g.setFont(new Font("arial", Font.BOLD, 20));
			g.drawString("Aperte espaço para reiniciar", 120, 520);
			
			g.dispose();
			g.clearRect(10, 10, WIDTH, HEIGHT);
		}
		
	}


	//Game loop
	@Override
	public void run() {
		while(run) {
			tick();
			repaint();
		}
		
	}


	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_SPACE) {
			score = 0;
			new NovoJogo();
		}
		if(e.getKeyCode() == KeyEvent.VK_RIGHT && !left) {
			right = true;
			up = false;
			down = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_LEFT && !right) {
			left = true;
			up = false;
			down = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_UP && !down) {
			up = true;
			right = false;
			left = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_DOWN && !up) {
			down = true;
			right = false;
			left = false;
		}
		
	}


	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}