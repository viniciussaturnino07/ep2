import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;

public class Fruit {
	
	private int cordX;
	private int cordY;
	private int width;
	private int height;
	private String tipo;
	private int score;
	
	public Fruit(int cordX, int cordY, int tileSize) {
		this.cordX = cordX;
		this.cordY = cordY;
		this.width = tileSize;
		this.height = tileSize;
		
		ArrayList<String> tipoFruta = new ArrayList<String>();
		tipoFruta.add("Simple");
		tipoFruta.add("Bomb");
		tipoFruta.add("Big");
		tipoFruta.add("Decrease");
		
		Random random = new Random();
		int r = random.nextInt(4);
		
		this.tipo = tipoFruta.get(r);
		
		//Pontuacao para cada tipo de fruta
		if(tipo == "Simple") {
			this.score = 1;
		}
		if(tipo == "Bomb") {
			this.score = 0;
		}
		if(tipo == "Big") {
			this.score = 2;
		}
		if(tipo == "Decrease") {
			this.score = 0;
		}
	}
	
	public Fruit(int cordX, int cordY, int tileSize,String tipo) {

		this.cordX = cordX;
		this.cordY = cordY;
		this.height = tileSize;
		this.width = tileSize;
		
		this.tipo = tipo;		
		
		
		if(tipo == "Simple") {
			this.score = 1;
		}
		else if(tipo == "Big") {
			this.score = 2;
		}
		else if(tipo == "Decrease") {
			this.score = 0;
		}
		else if(tipo == "Bomb") {
			this.score = 0;
		}
		else if(tipo == "Obstaculo") {
			this.score = 0;
		}
		
	}

	
	public String getTipo() {
		return tipo;
	}



	public void setTipo(String tipo) {
		this.tipo = tipo;
	}



	public int getScore() {
		return score;
	}



	public void setScore(int score) {
		this.score = score;
	}



	public void tick() {
		
	}
	
	//Funcao que desenha a fruta na tela
	public void draw(Graphics g) {
		//Simple Fruit
		if(tipo == "Simple") {
			g.setColor(Color.RED);
		}
		
		//Big Fruit
		if(tipo == "Big") {
			g.setColor(new Color(153, 51, 153));
		}
		
		//Bomb Fruit
		if(tipo == "Bomb") {
			g.setColor(Color.GRAY);
		}
		
		//Decrease
		if(tipo == "Decrease") {
			g.setColor(Color.YELLOW);
		}
		
		g.fillRect(cordX*width, cordY*height, width, height);
	}

	public int getCordX() {
		return cordX;
	}

	public void setCordX(int cordX) {
		this.cordX = cordX;
	}

	public int getCordY() {
		return cordY;
	}

	public void setCordY(int cordY) {
		this.cordY = cordY;
	}
	
}