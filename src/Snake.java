import java.awt.Color;
import java.awt.Graphics;

public class Snake {
	
	private int cordX;
	private int cordY;
	private int width;
	private int height;
	
	private String tipo = "Comum";
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Snake(int cordX, int cordY, int tileSize, String tipo) {
		this.cordX = cordX;
		this.cordY = cordY;
		this.width = tileSize;
		this.height = tileSize;
		this.tipo = tipo;
	}
	
	public void tick() {
		
	}
	
	//Funcao que desenha a cobra na tela
	public void draw(Graphics g) {
		//Cobra Comum
		if(this.tipo == "Comum") {
			g.setColor(Color.GREEN);
		}
		
		//Cobra Kitty
		else if(this.tipo == "Kitty") {
			g.setColor(new Color(255, 0, 127));
		}
		
		//Cobra Star
		else if(this.tipo == "Star") {
			g.setColor(Color.BLUE);
		}
		
		g.fillRect(cordX*width, cordY*height, width, height);
	}

	public int getCordX() {
		return cordX;
	}

	public void setCordX(int cordX) {
		this.cordX = cordX;
	}

	public int getCordY() {
		return cordY;
	}

	public void setCordY(int cordY) {
		this.cordY = cordY;
	}

}
